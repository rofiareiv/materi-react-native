import React, {Component} from 'react';
import {Alert, Text, View} from 'react-native';
import CustomButton from './component/CustomButton';

export class App extends Component {
  state = {
    name: '',
  };
  render() {
    return (
      <View>
        <Text style={{fontSize: 24}}> contoh aja </Text>
        <CustomButton label="Simpan" onTahan={() => Alert.alert('halo')} />
        <CustomButton label="Kembali" />
        <CustomButton
          label="Lanjut"
          onTahan={() => this.setState({name: 'kambing'})}
        />
        <Text>{this.state.name}</Text>
      </View>
    );
  }
}

export default App;
